
import '../styles/globals.css'
import Header from '../Components/Header/Header'
import Head from 'next/head'
import Footer from '../Components/Footer/Footer'
import { wrapper } from '../Components/store/createStore'
import { Provider } from 'react-redux';
import style from '../styles/Header.module.css'

const MyApp = ({ Component, pageProps }) => (
  <div className={style.maindiv} >
    <Head>
      <title>My Next App</title>
      <meta description="Helle there. This is the Next.js App" />
    </Head>
    <Header />
    <Component {...pageProps} />
    <Footer />
  </div>
);

export default wrapper.withRedux(MyApp);
