import React from "react";
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../Components/store/actions/Action_cart";
import Image from "next/image";
import style from '../../styles/Product.module.css'

export default function Post({ list }) {

  const router = useRouter()
  const dispatch = useDispatch();
  function clickhandle(list) {
    console.log("In click", list)
    router.push({
      pathname: '/buy_now',
      query: list,
    })
  }


  const mystate = useSelector(state => state.changeCartState)
  // console.log(mystate)
  function click_btn(list) {
    (mystate.find((item) => item.id === list.id)) ? alert("This item is already present in your cart") :
      dispatch(addToCart(list))
  }

  const mystle = {
    marginLeft: 60,
    marginTop: 20,
    width: 350,
    height: 50,
    borderRadius: 30,
    background: 'orange'
  }


  if(router.isFallback){
    return <h1>Loading.....</h1>
}

  return (

    <div>
    <div className={style.product} style={{
      textAlign: 'left',
      margin: 40,
      marginLeft: 60,
      padding: 20,
      display: 'flex',
      width: 850
    }}>

      <div style={{ width: 'auto', }} >

        <Image src={list.img1} width={400} height={250} style={{ borderStyle: 'solid', borderColor: '#A9A9A9', marginBottom: 10 }} alt={"No Image"} />
        <Image src={list.img2} width={400} height={250} style={{ borderStyle: 'solid', borderColor: '#A9A9A9', marginBottom: 10 }} alt={"No Image"} />
      </div>
      <div className="info" style={{ marginLeft: 60 }}>
        <h5>{list.name}</h5>
        <h6>{list.price}</h6>
        <p>{list.desc}</p>
        <div className="delivery">

          <p>* Free Delivery on orders over 500 Rs.</p>
          <p>* Cash on delivery available</p>
          <p>* 10 Days return Policy</p>
        </div>
        <button style={mystle} onClick={() => click_btn(list)} >Add To Cart</button>
        {/* <button  style={mystle}  >Add To Cart</button> */}
        <br></br>
        <button onClick={() => clickhandle(list)} style={mystle}>Buy Now</button>
      </div>
    </div>
  </div>
  );

}


export const getStaticProps = async (context) => {

  const { params } = context;
  const { id } = params
  const res = await fetch(`http://demo1875282.mockable.io/sunglasses/${id}`);
  let list = await res.json()
  list = list.sunglasses
  return {
    props: { list: list }
  }
}


export async function getStaticPaths() {
 
  const res = await fetch(`http://demo1875282.mockable.io/sunglasses`);
  let data = await res.json();
  data = data.sunglasses
  const paths = data.map(item => {
    return {
      params: { id: item.id.toString() }
    }
  })
  return {
    paths,
    fallback: true
  }
}

// import React from "react";
// import { useRouter } from 'next/router'
// import { useDispatch, useSelector } from "react-redux";
// import { addToCart } from "../../Components/store/actions/Action_cart";
// import Image from "next/image";
// import style from '../../styles/Product.module.css'

// export default function Post({ list }) {
//   console.log("This is list ", list)

//   const router = useRouter()
//   const dispatch = useDispatch();
//   function clickhandle(list) {
//     console.log("In click", list)
//     router.push({
//       pathname: '/buy_now',
//       query: list,
//     })
//   }


//   const mystate = useSelector(state => state.changeCartState)
//   // console.log(mystate)
//   function click_btn(list) {
//     (mystate.find((item) => item.id === list.id)) ? alert("This item is already present in your cart") :
//       dispatch(addToCart(list))
//   }

//   const mystle = {
//     marginLeft: 60,
//     marginTop: 20,
//     width: 350,
//     height: 50,
//     borderRadius: 30,
//     background: 'orange'
//   }


//   if(router.isFallback){
//     return <h1>Loading.....</h1>
// }

//   return (

//     <div>
//       <div className={style.product} style={{
//         textAlign: 'left',
//         margin: 40,
//         marginLeft: 60,
//         padding: 20,
//         display: 'flex',
//         width: 850
//       }}>

//         <div style={{ width: 'auto', }} >

//           <Image src={list.img1} width={400} height={250} style={{ borderStyle: 'solid', borderColor: '#A9A9A9', marginBottom: 10 }} alt={"No Image"} />
//           <Image src={list.img2} width={400} height={250} style={{ borderStyle: 'solid', borderColor: '#A9A9A9', marginBottom: 10 }} alt={"No Image"} />
//         </div>
//         <div className="info" style={{ marginLeft: 60 }}>
//           <h5>{list.name}</h5>
//           <h6>{list.price}</h6>
//           <p>{list.desc}</p>
//           <div className="delivery">

//             <p>* Free Delivery on orders over 500 Rs.</p>
//             <p>* Cash on delivery available</p>
//             <p>* 10 Days return Policy</p>
//           </div>
//           <button style={mystle} onClick={() => click_btn(list)} >Add To Cart</button>
//           {/* <button  style={mystle}  >Add To Cart</button> */}
//           <br></br>
//           <button onClick={() => clickhandle(list)} style={mystle}>Buy Now</button>
//         </div>
//       </div>
//     </div>
//   );

// }


// export const getStaticProps = async (context) => {

//   const { params } = context;
//   const { id } = params
//   const res = await fetch(`http://demo1875282.mockable.io/sunglasses/${id}`);
//   let list = await res.json()
//   list = list.sunglasses
//   return {
//     props: { list: list }
//   }
// }


// export async function getStaticPaths() {
 
//   const res = await fetch(`http://demo1875282.mockable.io/sunglasses`);
//   let data = await res.json();
//   data = data.sunglasses
//   const paths = data.map(item => {
//     return {
//       params: { id: item.id.toString() }
//     }
//   })
//   return {
//     paths,
//     fallback: true
//   }
// }