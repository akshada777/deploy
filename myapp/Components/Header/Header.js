import mystyle from '../../styles/Header.module.css';
import Link from 'next/link'
import React from 'react'
import Image from 'next/image';
import img from '../../pages/img/spect.png'
export default function Header() {

 
  return (
    <div>

      <header className={mystyle.header}>

        <nav className={mystyle.nav}  >


          <Link  href="/" exact ><a className={mystyle.link}>Home</a></Link>
          <Link href="/cart" ><a className={mystyle.link} >Cart</a></Link>
          <Link href="/aboutus" ><a className={mystyle.link} >About Us</a></Link>
          <Link href="/contact" ><a className={mystyle.link} >Contact Us</a></Link>

        </nav>
      </header>
    </div>
  );
}
