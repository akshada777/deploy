
const changeCartState = (state = [], action) => {
    switch (action.type) {
        case 'add_cart':
            // console.log("In add to cart reducer", action.payload.item)
            alert("Item is added in your cart")
            // state = [...state, action.payload.item.list]
            // console.log(state)
            return  [...state, action.payload.item]
            break;
        case 'inc_qnt':
                return state.map((d)=>{
                    if(d.id===action.payload.id)
                    {
                        return {...d, qnt:String(Number(d.qnt)+1)}
                        // d.qnt=String(Number(d.qnt)+1)
                        // console.log(d.id," *** ", action.payload.id)
                    }
                    return d;
                })
                break;
        case 'dec_qnt' :
           return state.map((d)=>{
                if(d.id===action.payload.id)
                {
                    return {...d, qnt:String(Number(d.qnt)-1)}
                   
                }
                return d;

            })
            
            break;
        case 'remove_item':
            return state= state.filter(
                ((d)=>d.id!==action.payload.myid)
            );
            break;
        default:
            return state
        
    }
    return state;
   
}


export default changeCartState
