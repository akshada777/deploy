import { combineReducers } from "redux";
import data_sunglass from "./SunglassesReducer";
import data_frame from "./FrameReducer";
import changeCount from "./ChangeCount";
import changeCartState from "./ChangeCartState";
import changeBuyNowState from "./ChangeBuyNow";

const RootReducer = combineReducers({
    data_sunglass:data_sunglass,
    data_frame:data_frame,
     changeCount,
    changeCartState,
    changeBuyNowState
})
export default RootReducer
