import React from 'react'
// import { useState, useEffect} from 'react'
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {update_qnt_inc,update_qnt_dec,remove_item} from  '../store/actions/Action_cart'; 
import Image from "next/image";

export default function Cart_items({item}) {

    
    // const state=useSelector((state)=> state.changeCartState)
    const dispatch = useDispatch();
    const history=useHistory()
    console.log("In cart Item ",item)

    const layout = {
        borderStyle:'solid',
        borderWidth:1,
        paddingLeft:20,
        marginTop: 10,
        display: 'flex',
        marginLeft: 25,
        width: 700
    }
    const mystyle = {
        background:'none',
        border:'none',
        color: 'black',
        fontSize: 20,
        marginTop:-10
    }
    const btn_style= {
        height:30
    }

    

    return (
       
        <div style={{display:'flex' }} >
        {/* <center><div style={{ background: 'rgb(21, 155, 148)', width: 900, height: 60, color: 'white', fontSize: 30 }}><b>Your Item</b></div></center> */}
        <div id="info" style={layout}>
            <div>
                <Image alt="No Photo" id="info" src={item.img1} height= {150} width= {150} ></Image>

                <div style={{display:'flex',marginLeft:40}}>
                  
                    <button style={btn_style} onClick={() => dispatch(update_qnt_inc(item.id)) } >+</button>
                   
                    <p style={{
                        fontSize:20,
                        width:'max-content',
                        marginLeft:10,
                        marginRight:10
                        
                    }} >{item.qnt}</p>
                   
                        {(item.qnt>=(1))?
                        <button style={btn_style} onClick={() =>  dispatch(update_qnt_dec(item.id))} >-</button>:
                        <button style={btn_style} >-</button>
                            
                    }
                   
                    {/* <button style={btn_style} onClick={() =>  dispatch(update_qnt_dec(item.id,(item.qnt-1)))} >-</button> */}
                    
                </div>
            </div>


            <div id="info" style={{ marginTop: 40 }}>
                <h5 id="info" >{item.name}</h5>
                <b><p id="info" >{item.price}</p></b>
                
                <button onClick={()=> dispatch(remove_item(item.id)) } style={mystyle}><b>Remove</b></button>
              
                   
            </div>
         
        </div>
       
    </div>

    )
}


// import React from 'react'
// // import { useState, useEffect} from 'react'
// import { useHistory } from 'react-router-dom';
// import { useSelector, useDispatch } from 'react-redux';
// import {update_qnt_inc,update_qnt_dec,remove_item} from  "../actions/Action_cart"; 

// import Cart_List from './Cart_List';

// export default function Cart_items({item}) {

    
//     const state=useSelector((state)=> state.changeCartState)
//     const dispatch = useDispatch();
//     const history=useHistory()

//     // setTimeout( state.map((d)=>{
//     //     if(d.id===item.id)
//     //     {
//     //         item.qnt=d.qnt
//     //     }
//     // }), 3000); 
   
//     const handleClick=(item)=>{
        
//     }

//     const layout = {
//         borderStyle:'solid',
//         borderWidth:1,
//         paddingLeft:20,
//         marginTop: 20,
//         display: 'flex',
//         marginLeft: 250,
//         width: 800
//     }
//     const mystyle = {
//         background:'none',
//         border:'none',
//         color: 'black',
//         fontSize: 20
//     }
//     const btn_style= {
//         height:30
//     }

    

//     return (
       
//         <div>
//         {/* <center><div style={{ background: 'rgb(21, 155, 148)', width: 900, height: 60, color: 'white', fontSize: 30 }}><b>Your Item</b></div></center> */}
//         <div id="info" style={layout}>
//             <div>
//                 <img alt="No Photo" id="info" src={item.img1} style={{ height: 200, width: 200 }} ></img>
//                 <br></br>
//                 <div style={{display:'flex',marginLeft:40}}>
                  
//                     <button style={btn_style} onClick={() => dispatch(update_qnt_inc(item.id,(item.qnt+1))) } >+</button>
                   
//                     <p style={{
//                         fontSize:20,
//                         width:'max-content',
//                         marginLeft:10,
//                         marginRight:10
                        
//                     }} >{item.qnt}</p>
//                     <button style={btn_style} onClick={() =>  dispatch(update_qnt_dec(item.id,(item.qnt-1)))} >-</button>
//                 </div>
//             </div>


//             <div id="info" style={{ marginTop: 80 }}>
//                 <h5 id="info" >{item.name}</h5>
//                 <b><p id="info" >{item.price}</p></b>
//                 <br></br><br></br>
//                     <button onClick={()=> dispatch(remove_item(item.id)) } style={mystyle}>Remove</button>
              
//             </div>
//         </div>
//         <div>
//         </div>
                 
//     </div>

//     )
// }
