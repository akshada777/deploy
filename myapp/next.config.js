module.exports = {
  // reactStrictMode: true,
 
  images: {
    domains:["https://lenskart.com",
    "https://static.lenskart.com",
    "static.lenskart.com",
    "static1.lenskart.com",
    "static2.lenskart.com",
    "https://static1.lenskart.com",
    "https://static2.lenskart.com"]
  },
}

// const withImages = require('next-images')
// module.exports = withImages({
//   assetPrefix: 'https://example.com',
//   dynamicAssetPrefix: true,
//   webpack(config, options) {
//     return config
//   }
// })